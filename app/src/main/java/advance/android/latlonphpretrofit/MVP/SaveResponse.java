package advance.android.latlonphpretrofit.MVP;

public class SaveResponse {
    private String action;

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    @Override
    public String toString() {
        return
                "SaveResponse{" +
                        "action = '" + action + '\'' +
                        "}";
    }
}
