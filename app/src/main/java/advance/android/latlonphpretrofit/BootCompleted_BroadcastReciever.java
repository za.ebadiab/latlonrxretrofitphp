package advance.android.latlonphpretrofit;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import advance.android.latlonphpretrofit.utils.PublicMethods;

public class BootCompleted_BroadcastReciever extends BroadcastReceiver {
    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            PublicMethods.toast("Intent.ACTION_BOOT_COMPLETED");
            Intent intentService = new Intent(context, LatLonService.class);
            context.startService(intentService);
        }
    }
}
