package advance.android.latlonphpretrofit;

import android.Manifest;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import advance.android.latlonphpretrofit.MVP.Contracts;
import advance.android.latlonphpretrofit.MVP.Presenter;
import advance.android.latlonphpretrofit.utils.PublicMethods;


public class ActivityMain extends AppCompatActivity implements Contracts.View {

    Presenter presenter = new Presenter(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getPermission();
    }

    void startLocationService() {
        Intent intent = new Intent(this, LatLonService.class);
        startService(intent);

    }


    void getPermission() {


        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION

                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {

                startLocationService();
                PublicMethods.toast("GPS permitted!");
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {


            }

        }).check();

    }


}
