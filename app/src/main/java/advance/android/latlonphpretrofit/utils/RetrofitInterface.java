package advance.android.latlonphpretrofit.utils;

import advance.android.latlonphpretrofit.MVP.SaveResponse;
import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface RetrofitInterface {


    @GET("LatLonSave.php/")
    Observable<SaveResponse> latlonSave(
            @Query("lat") double latitude,
            @Query("lon") double longitude,
            @Query(value = "location_date", encoded = true) String location_date
    );


//    @FormUrlEncoded
//    @POST("LatLonSave.php")
//    Observable<SaveResponse> latlonSave(
//            @Field("lat") double latitude,
//            @Field("lon") double longitude,
//          //  @Field(value = "location_date" , encoded=true) String location_date
//            @Url String location_date
//    ) ;


}
