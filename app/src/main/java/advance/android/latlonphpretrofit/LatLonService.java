package advance.android.latlonphpretrofit;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import advance.android.latlonphpretrofit.MVP.SaveResponse;
import advance.android.latlonphpretrofit.utils.PublicMethods;
import advance.android.latlonphpretrofit.utils.RetrofitGenerator;
import advance.android.latlonphpretrofit.utils.RetrofitInterface;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationAccuracy;
import io.nlopez.smartlocation.location.config.LocationParams;
import io.nlopez.smartlocation.rx.ObservableFactory;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LatLonService extends Service {


    @Override
    public void onCreate() {
        super.onCreate();
        //     getLocation();
        getLocation2();

    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
     //     return super.onStartCommand(intent, flags, startId);
        return START_STICKY;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
     //   onCreate();
    }

    void getLocation2() {
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        PublicMethods.toast("before samrtLocation");
                        //  saveRetroLocation(8.4, 8.6 , getCurrentDate());
                        SmartLocation.with(LatLonService.this)
                                .location()
                                .oneFix()
                                .start(new OnLocationUpdatedListener() {

                                    @Override
                                    public void onLocationUpdated(Location location) {
                                        PublicMethods.toast("in samrtLocation");
                                        //send location to server
                                        //          Toast.makeText(LatLonService.this, "lat= "+location.getLatitude()+" , lon= "+location.getLongitude()+" date= "+getCurrentDate(), Toast.LENGTH_LONG).show();
                                        saveRetroLocation(location.getLatitude(), location.getLongitude(), getCurrentDate());
                                    }
                                });
                        getLocation2();
                    }
                }
                , 5000);

    }


    void getLocation() {


        long mLocTrackingInterval = 1000 * 5; // 5 sec
        float trackingDistance = 0;

        LocationAccuracy trackingAccuracy = LocationAccuracy.HIGH;

        LocationParams.Builder builder = new LocationParams.Builder()
                .setAccuracy(trackingAccuracy)
                .setDistance(trackingDistance)
                .setInterval(mLocTrackingInterval);

        //   saveRetroLocation(8.4, 8.6 , getCurrentDate());
        SmartLocation.with(this)
                .location()
                .continuous()
                .config(builder.build())
                .start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        //   Toast.makeText(LatLonService.this, "lat= " + location.getLatitude() + " , lon= " + location.getLongitude() + " date= " + getCurrentDate(), Toast.LENGTH_LONG).show();
                        saveRetroLocation(location.getLatitude(), location.getLongitude(), getCurrentDate());
                    }
                });


    }

    String getCurrentDate() {


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        try {
            return URLEncoder.encode(sdf.format(new Date()), "UTF-8");
            //      return sdf.format(new Date());
        } catch (Exception e) {
            return "";
        }


    }

    //    void getLocation3(){
//
//        Observable<Location> locationObservable = ObservableFactory.from(SmartLocation.with(LatLonService.this).location());
//        locationObservable.subscribe(new Action1<Location>() {
//            @Override
//            public void call(Location location) {
//                // Do your stuff here :)
//            }
//        });
//    }


    @SuppressLint("CheckResult")
    void saveRetroLocation(double lat, double lon, String location_date) {
        RetrofitInterface retrofitInterface = RetrofitGenerator.createService(RetrofitInterface.class);
        retrofitInterface.latlonSave(lat, lon, location_date)
                .subscribeOn(Schedulers.io())
                .observeOn(io.reactivex.android.schedulers.AndroidSchedulers.mainThread())
                .subscribe(new Observer<SaveResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(SaveResponse saveResponse) {

                    }

                    @Override
                    public void onError(Throwable e) {

                        Toast.makeText(LatLonService.this, "error= " + e, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }


}
